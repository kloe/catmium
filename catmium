#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

if [ "$#" -lt 1 ]; then
    cat "$(dirname "$0")/README" >> /dev/stderr
    exit 1
fi

cat <<'EOF'
(function() {
    var pending = new Map();
    var modules = new Map();

    function require(selfAbsPath, otherRelPath) {
        var otherAbsPath;
        if (selfAbsPath === "") {
            otherAbsPath = otherRelPath;
        } else if (otherRelPath.startsWith("../")) {
            otherAbsPath =
                selfAbsPath.replace(/(\/[^\/]*){2}$/, '') +
                otherRelPath.replace(/^\.\./, '');
        } else if (otherRelPath.startsWith("./")) {
            otherAbsPath =
                selfAbsPath.replace(/(\/[^\/]*){1}$/, '') +
                otherRelPath.replace(/^\./, '');
        } else {
            otherAbsPath = otherRelPath;
        }
        if (pending.has(otherAbsPath)) {
            function innerRequire(path) {
                return require(otherAbsPath, path);
            }
            var module = {exports: {}};
            pending.get(otherAbsPath)(innerRequire, module, module.exports);
            modules.set(otherAbsPath, module.exports);
            pending.delete(otherAbsPath);
        }
        if (!modules.has(otherAbsPath)) {
            throw Error(selfAbsPath + ": require: No such module: " + otherAbsPath);
        }
        return modules.get(otherAbsPath);
    }

    function define(selfAbsPath, body) {
        pending.set(selfAbsPath, body);
    }
EOF

for selfAbsPath in "${@:2}"; do
    cat <<EOF
    define("$selfAbsPath", function(require, module, exports) {
EOF
    cat "$selfAbsPath"
    cat <<'EOF'
    });
EOF
done

cat <<EOF
    require("", "$1");
EOF

cat <<'EOF'
})();
EOF
