"use strict";

exports.log = function(result) {
    return function(string) {
        return function() {
            console.log(string);
            return result;
        };
    };
};
