module Main
  ( Effect
  , main
  ) where

import Data.Show (show)
import Data.Unit (Unit (..))

foreign import data Effect :: Type -> Type

main :: Effect Unit
main = log Unit (show Unit)

foreign import log :: forall a. a -> String -> Effect a
