module Data.Unit
  ( Unit (..)
  ) where

import Data.Show (class Show)

data Unit = Unit

instance showUnit :: Show Unit where
  show Unit = "Unit"
