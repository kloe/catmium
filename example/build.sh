#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

(cd example && purs compile $(find . -name '*.purs'))
./catmium example/src/entry.js example/src/entry.js example/output/*/*.js > target/example.js
cp example/index.html target

echo 'Example compiled to target directory.'
